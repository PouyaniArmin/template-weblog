<?php  ?>
<div class="w-75 pt-4 container mx-auto shadow mt-4 py-4">
    <header class="text-center">
        <h1>Register</h1>
    </header>
    <div class="container w-75">
        <form method="post">
            <div class="py-1">
                <label for="validationServer01" class="form-label">Username</label>
                <input type="text" name="username" class="form-control" id="validationServer01">
                <div class="feedback text-danger">
                    <?php echo $username ?>
                </div>
            </div>
            <div class="py-1">
                <label for="validationTooltip02" class="form-label">Email</label>
                <input name="email" type="text" class="form-control" id="validationTooltip02">
                <div class="feedback text-danger">
                    <?php echo $email?>
                </div>
            </div>
            <div class="py-1">
                <label for="validationTooltipUsername" class="form-label">Password</label>
                <input name="password" type="text" class="form-control">
                <div class="feedback text-danger">
                    <?php  echo $password?>
                </div>
            </div>
            <div class="py-">
                <label for="validationTooltip03" class="form-label">Confirm Password</label>
                <input name="confirmPassword" type="text" class="form-control">
                <div class="feedback text-danger">
                    <?php echo $confirmPassword?>
                </div>
            </div>
            <div class="py-2">
                <button class="btn btn-primary" type="submit">Submit form</button>
            </div>
        </form>
    </div>

</div>