<?php

namespace TemplateWeblog\Controllers;

use BadMethodCallException;
use TemplateWeblog\Core\Application;
use TemplateWeblog\Core\Controller;
use TemplateWeblog\Core\Request;
use TemplateWeblog\Core\Router;
use TemplateWeblog\Model\User;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $fields = [
            'username' => 'required | min:5 | max:10',
            'email' => 'required | email',
            'password' => 'required | min:6 | max:18',
            'confirmPassword' => 'required | same:password'
        ];
        if ($request->validate($fields)) {
            echo "register";
        }

        return $this->view('home');
    }
}
