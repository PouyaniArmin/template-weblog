<?php

namespace TemplateWeblog\Database;

class MysqlQuery
{
    /**
    * @param string $table get table name database
    * @param array $params data for CRUD 
    * @return string query mysql
    */ 
    protected function selectQuery(string $table, array $params = []): string
    {
        return $this->queryBuilder('select', $table, $params);
    }

    protected function insertQuery(string $table, array $params): string
    {
        return $this->queryBuilder('insert', $table, $params);
    }

    protected function updateQuery(string $table, array $params = []): string
    {
        return $this->queryBuilder('update', $table, $params);
    }

    protected function deleteQuery(string $table, array $params = []): string
    {
        return $this->queryBuilder('delete', $table, $params);
    }

    /** @return string Returns the string according to the position(selec,insert,update,delete) */
    private function queryBuilder($query, $table, $params = []): string
    {
        switch ($query) {
            case 'select':
                if (count($params) > 0) {
                    return "SELECT * FROM $table WHERE {$this->whereQuery($params)} = :{$this->whereQuery($params)}";
                    break;
                } else {
                    return "SELECT * FROM $table";
                    break;
                }
            case 'insert':
                return "INSERT INTO $table ( {$this->whereQuery($params)} ) VALUES ( :{$this->valuesQuery($params)} ) ;";
                break;
            case 'update':
                return "UPDATE $table SET {$this->updateWhereQuery($params)} WHERE id = :id";
                break;
            case 'delete':
                return "DELETE FROM $table WHERE {$this->whereQuery($params)} = :{$this->whereQuery($params)}";
                break;
        }
    }

    /** @param array Creating the WHERE section in the query 
     * @return string where section
     * */    
    private function whereQuery(array $params): string
    {
        $key = array_keys($params);
        return implode(' , ', $key);
    }

    /** @param array Creating the VALUES section in the query
     * * @return string where section
     */
    private function valuesQuery(array $params): string
    {
        $key = array_keys($params);
        return implode(' , :', $key);
    }

    private function updateWhereQuery($params): string
    {
        $key = array_keys($params);
        foreach ($key as $item) {
            $set[] = "$item = :$item";
        }
        $att = implode(" , ", $set);
        return $att;
    }
}
