<?php

namespace TemplateWeblog\Core;


abstract class Model extends Database
{
    // return  string table name for class Database
    abstract public function table(): string;
    public function __construct()
    {
        parent::__construct();
        $this->tableName($this->table());
    }

}
