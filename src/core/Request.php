<?php

namespace TemplateWeblog\Core;

class Request extends Validation
{

    /**
     * @return string This function removes the question mark and then 
     * returns the current URL as a string.
     */
    public function path()
    {
        $path = $_SERVER['REQUEST_URI'];
        $position = strpos($path, "?");
        if ($position) {
            $path = substr($path, 0, $position);
        }
        return $path;
    }
    /**
     * @return string This function returns the Method HTTP as a string.
     */
    public function method()
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }
    /**
     * @return bool This function checks the  post and get Method HTTP.
     */
    public function isGET(): bool
    {
        return $this->method() === 'get';
    }
    public function isPost(): bool
    {
        return $this->method() === 'post';
    }
    /**
     * @return array This function returns the value of the form.
     * @param filter_input To prevent an XSS attack.
     */
    public function body(): array
    {
        $data = [];
        if ($this->method() === 'get') {
            foreach ($_GET as $key => $value) {
                $data[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        if ($this->method() === 'post') {
            foreach ($_POST as $key => $value) {
                $data[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        return $data;
    }

    // validation rules
    public function validate($fields)
    {
        if ($this->isPost()) {

            $this->validatitor($this->body(), $fields);
            if (!count($this->errors) > 0) {
                return true;
            }
        }
        return false;
    }
}
