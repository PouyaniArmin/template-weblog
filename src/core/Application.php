<?php

namespace TemplateWeblog\Core;

use PDO;
use PDOException;
use TemplateWeblog\Controllers\HomeController;
use TemplateWeblog\Model\User;
use Throwable;

class Application
{
    public static string $ROOT_PATH;
    public Request $request;
    public Router $router;
    public static Application $app;
    public Controller $controller;
    public function __construct($path)
    {
        self::$app=$this;
        self::$ROOT_PATH = $path;
        $this->request = new Request;
        $this->router = new Router($this->request);
    }
    // require config Router
    public function config(){
        require "../src/router/webConfig.php";
    }
    public function getConterller():Controller{
        return $this->controller;
    }
    public function setController(Controller $controller){
        $this->controller=$controller;
    }


    public function run()
    {
        $this->config();
       echo $this->router->resolve();
    }
}
