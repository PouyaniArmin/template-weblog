<?php

namespace TemplateWeblog\Core;

use TemplateWeblog\Controllers\HomeController;
use Throwable;

class Controller
{

    public string $layout;
    protected function view($view, $params = [])
    {
        return Application::$app->router->renderView($view, $params);
    }
    protected function setLayout($layout): string
    {
        return $this->layout = $layout;
    }
}
