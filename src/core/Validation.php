<?php

namespace TemplateWeblog\Core;

class Validation
{

    // validation error message
    private const DEFAULT_VALIDATION_ERRORS = [
        'required' => 'Please enter the %s',
        'email' => 'The %s is not a valid Email address',
        'min' => 'The %s must have at least %s characters',
        'max' => 'The %s must have at most %s characters',
        'between' => 'The %s must have between %d and %d characters',
        'same' => 'The %s must match with %s',
        'secure' => 'The %s must have between 8 and 64 characters and contain at least one number, one upper case letter, one lower case letter and one special character',
    ];

    public array $errors = [];

    // validtion rules 
    protected function validatitor($data, $fields): array
    {
        $split = fn ($str, $sperartor) => array_map('trim', explode($sperartor, $str));

        foreach ($fields as $field => $option) {
            $rules = $split($option, "|");
            foreach ($rules as $rule) {
                $params = [];
                if (strpos($rule, ":")) {
                    [$rule_name, $param_str] = $split($rule, ":");
                    $params = $split($param_str, ',');
                } else {
                    $rule_name = trim($rule);
                }
                $fn = 'is_' . $rule_name;
                if (is_callable([$this, $fn])) {
                    $pass = call_user_func_array([$this, $fn], [$data, $field, ...$params]);
                    if (!$pass) {
                        $this->errors[$field] = sprintf(self::DEFAULT_VALIDATION_ERRORS[$rule_name], $field, ...$params);
                    }
                }
            }
        }
        return $this->errors;
    }


    private function is_required(array $data, string $field): bool
    {
        return isset($data[$field]) && trim($data[$field]) !== '';
    }
    private function is_email(array $data, string $field): bool
    {
        if (empty($data[$field])) {
            return true;
        }
        return filter_var($data[$field], FILTER_VALIDATE_EMAIL);
    }


    private function is_min(array $data, string $field, int $min): bool
    {
        if (empty($data[$field])) {
            return true;
        }
        if (!isset($data[$field])) {
            return true;
        }
        return strlen($data[$field]) >= $min;
    }

    private function is_max(array $data, string $field, int $max): bool
    {
        if (empty($data[$field])) {
            return true;
        }
        if (!isset($data[$field])) {
            return true;
        }
        return strlen($data[$field]) <= $max;
    }

    private function is_same(array $data, string $field, string $other): bool
    {
        if (isset($data[$field], $data[$other])) {
            return $data[$field] === $data[$other];
        }
        if (!isset($data[$field]) && !isset($data[$other])) {
            return true;
        }
        return false;
    }
    private function is_between(array $data, string $field, int $min, int $max): bool
    {
        if (!isset($data[$field])) {
            return $data[$field];
        }
        $len = strlen($data[$field]);
        return $len >= $min && $len <= $max;
    }

    private function is_secure(array $data, string $field): bool
    {
        if (!isset($data[$field])) {
            return false;
        }
        $pattern = "#.*^(?=.{8,64})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$#";
        return preg_match($pattern, $data[$field]);
    }
}
