<?php

namespace TemplateWeblog\Core;

use Closure;
use Exception;

class Router
{
    protected static array $routes = [];

    protected Request $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param $url string The variable is assigned as a key in the presentation
     * @param $callback Closure|array The Closure|array is assigned as a value in the presentation
     */
    public static function get(string $url, Closure|array $callback): void
    {
        self::$routes['get'][$url] = $callback;
    }

    public static function post(string $url, Closure|array $callback): void
    {
        self::$routes['post'][$url] = $callback;
    }

    public function resolve()
    {
        $method = $this->request->method();
        $url = $this->request->path();

        if (isset(self::$routes[$method])) {
            foreach (self::$routes[$method] as $routeUrl => $callback) {
                //creating a controller to call the class and method.
                if (is_array($callback)) {
                    Application::$app->controller = new $callback[0];
                    $callback[0] = Application::$app->controller;
                }
                if ($routeUrl === $url) {
                    return call_user_func($callback, $this->request);
                }
                $pattern = preg_replace('/\/:([^\/]+)/', '/(?P<$1>[^/]+)', $routeUrl);
                if (preg_match('#^' . $pattern . '$#', $url, $matches)) {
                    $params = array_filter($matches, 'is_string', ARRAY_FILTER_USE_KEY);
                    return call_user_func_array($callback, $params);
                }
            }
            return $this->notFoundPage();
        }
    }

    // makeing view
    public function renderView($view, $params = [])
    {
        $layout = $this->renderLayout();
        $content = $this->renderOnlyView($view, $params);
        return str_replace("{{content}}", $content, $layout);
    }
    // set layout
    private function renderLayout()
    {
        // by defulat initilaze layout variable
        $layout = Application::$app->controller->layout ?? 'main';
        ob_start();
        require Application::$ROOT_PATH . "/src/view/layout/" . $layout . '.php';
        return ob_get_clean();
    }

    // set view 
    // pass data to view
    private function renderOnlyView($view, array $params)
    {
        // pass error message to form
        if (isset($this->request->errors) && count($this->request->errors) > 0) {
            $params = $this->request->errors;
        }
        foreach ($params as $key => $value) {
            $$key = $value;
        }
        ob_start();
        require Application::$ROOT_PATH . "/src/view/$view.php";
        return ob_get_clean();
    }

    // show notFound page send code 404 to response_code_http
    private function notFoundPage()
    {
        http_response_code(404);
        return $this->renderView('notFound');
    }
}
