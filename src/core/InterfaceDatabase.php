<?php

namespace TemplateWeblog\Core;
interface InterfaceDatabase
{

    /**
     * @param array $where By default, the query will return all the data, if you need specific data, set the where[key=>value] value.
     * @throws $th If something interesting cannot happen 
     */
    public function select($where = []);

    /**
     * @param array $parameters Store data in a table
     * @throws $th If something interesting cannot happen 
     */
    public function insert($parameters = []);

    /**
     * @param array $parameters Update data in a table
     * @throws $th If something interesting cannot happen 
     */
    public function update($parameters = []);

    /**
     * @param array $parameters Delete data in a table
     * @throws $th If something interesting cannot happen 
     */
    public function delete($parameters = []);
}
