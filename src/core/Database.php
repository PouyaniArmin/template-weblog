<?php

namespace TemplateWeblog\Core;

use Dotenv\Dotenv;
use Exception;
use PDO;
use PDOException;
use TemplateWeblog\Database\MysqlQuery;

class Database extends MysqlQuery implements InterfaceDatabase
{
    private $connection = null;
    private $tableName = null;

    public function __construct()
    {
        $dotenv = Dotenv::createImmutable(Application::$ROOT_PATH);
        $dotenv->safeLoad();
        try {
            $dns = "mysql:host={$_ENV['HOST_NAME']};dbname={$_ENV['DATABASE_NAME']}";
            $this->connection = new PDO($dns, $_ENV['DATABASE_USER'], $_ENV['DATABASE_PASSWORD']);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $pe) {
            throw new Exception("Connection Failed {$pe->getMessage()}");
        }
    }

    /** @param string $tableName set table name database 
     * initialize in model calss
     */
    protected function tableName($tableName)
    {
        $this->tableName = $tableName;
    }
    public function select($where = [])
    {
        try {
            $statment = $this->selectQuery($this->tableName, $where);
            $stmt = $this->executedStatment($statment, $where);
            return $stmt->fetchAll();
        } catch (\Throwable $th) {
            throw new Exception("Error Processing Select {$th->getMessage()}");
        }
    }
    public function insert($parameters = [])
    {
        try {
            $statment = $this->insertQuery($this->tableName, $parameters);
            $this->executedStatment($statment, $parameters);
            return $this->connection->lastInsertId();
        } catch (\Throwable $th) {
            throw new Exception("Error Connection Database Insert Failed {$th->getMessage()}");
        }
    }

    public function update($parameters = [])
    {
        try {
            $statment = $this->updateQuery($this->tableName, $parameters);
            $this->executedStatment($statment, $parameters);
        } catch (\Throwable $th) {
            throw new Exception("Error Connection Database SELECT Failed {$th->getMessage()}");
        }
    }
    public function delete($parameters = [])
    {
        try {
            $statment = $this->deleteQuery($this->tableName, $parameters);
            $this->executedStatment($statment, $parameters);
        } catch (\Throwable $th) {
            throw new Exception("Error Connection Database SELECT Failed {$th->getMessage()}");
        }
    }

    /**
     * @param string $stament Prepare the query for CRUD 
     * @param array $parameters execute data
     * @throws $th If something interesting cannot happen 
     */
    private function executedStatment($statment = "", $parameters = [])
    {
        try {
            $stmt = $this->connection->prepare($statment);
            $stmt->execute($parameters);
            return $stmt;
        } catch (\Throwable $th) {
            throw new Exception("Error Processing Execute function Database {$th->getMessage()}");
        }
    }
}
