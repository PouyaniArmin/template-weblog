<?php

use TemplateWeblog\Core\Application;
require __DIR__."/../vendor/autoload.php";
require "./router/webConfig.php";
$app=new Application(dirname(__DIR__));
$app->run();
