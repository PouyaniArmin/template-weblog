<?php 

namespace TemplateWeblog\Model;

use TemplateWeblog\Core\Model;

class User extends Model{
    public function table(): string
    {
        return 'users';
    }
}